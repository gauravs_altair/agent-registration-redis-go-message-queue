package models

import (
	"encoding/json"
	"fmt"
)

type Agent struct {
	Hostname   string `json:"hostname"`
	State      string `json:"state"`
	Continent  string `json:"continent"`
	VendorName string `json:"vendorname"`
	AgentID    string `json:"agent_id"`
}

type AgentMessage struct {
	Msg      string
	Hostname string
	ID       string
}

func (a *AgentMessage) MarshalData() ([]byte, error) {
	return json.Marshal(a)
}

func (a *AgentMessage) UnmarshalData(msg []byte) error {

	if err := json.Unmarshal(msg, &a); err != nil {
		fmt.Println(err)
	}

	return nil
}
