package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	AgentModels "go-redis-server/Agent/models"
	"go-redis-server/models"
	"go-redis-server/utils"
	"log"
	"os"
	"os/exec"

	"github.com/go-redis/redis/v8"
	"github.com/spf13/viper"
)

func main() {

	// redis

	rbd := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	fmt.Print("rbd: ", rbd)

	ctx := context.Background()

	// query data
	// vi := viper.New()

	viper.SetConfigName("vendor")

	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		fmt.Print(err)
	}

	var vendor AgentModels.VendorConfig

	if err := viper.Unmarshal(&vendor); err != nil {
		fmt.Println(err)
	}

	fmt.Println("# Viper scan done")

	fmt.Println("Query: ", vendor.Vendor.QueryTool, " ", vendor.Vendor.Params, " ", vendor.Vendor.Uid)

	cm := exec.Command("cmd", "/C", vendor.Vendor.QueryTool, "-licstatxml", "-hhwu", "-uid", vendor.Vendor.Uid)

	var out bytes.Buffer

	cm.Stdout = &out
	err := cm.Run()
	if err != nil {
		log.Fatal(err)
	}
	// fmt.Println(out.String())
	name, e := os.Hostname()

	if e != nil {
		fmt.Println(e)
	}
	msg := models.AgentMessage{Msg: out.String(), Hostname: name}

	val, err := json.Marshal(msg)

	if err != nil {
		panic(err)
	}

	utils.PublishMessage(rbd, &ctx, val, "channel1")

	channel := rbd.Subscribe(ctx, "channel2")

	// print acknownledgement
	for msg := range channel.Channel() {
		a := &models.AgentMessage{}
		e := a.UnmarshalData([]byte(msg.Payload))

		if e != nil {
			fmt.Println(e)
		}

		fmt.Println("Unique ID : ", a.ID, "Hostname: ", a.Hostname)
	}

	// for i := 0; i < 100; i++ {

	// 	agentInfo := models.Agent{Hostname: "com.altair.sao", State: "UP", Continent: "EMEA", VendorName: "Dyana", AgentID: strconv.Itoa((i))}

	// 	msg, err := json.Marshal(agentInfo)
	// 	if err != nil {
	// 		panic(err)
	// 	}

	// 	utils.PublishMessage(rbd, &ctx, string(msg), "channel1")
	// 	time.Sleep(3 * time.Second)
	// 	utils.PrintMessage(channel, &ctx)
	// }

}
