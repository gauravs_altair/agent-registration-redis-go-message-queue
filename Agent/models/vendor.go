package models

type Configuration struct {
	Frequency int `mapstructure:"frequency"`
}

type Vendor struct {
	Name      string `mapstructure:"name"`
	Method    string `mapstructure:"method"`
	Uid       string `mapstructure:"uid"`
	QueryTool string `mapstructure:"querytool"`
	Params    string `mapstructure:"params"`
}

type VendorConfig struct {
	Version       float32       `mapstructure:"version"`
	Configuration Configuration `mapstructure:"configuration"`
	Vendor        Vendor        `mapstructure:"vendor"`
}
