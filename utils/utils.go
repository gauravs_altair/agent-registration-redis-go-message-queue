package utils

import (
	"context"
	"fmt"
	"go-redis-server/db"
	"go-redis-server/models"

	"github.com/go-redis/redis/v8"
)

var DB = db.Connect("localhost", "5432", "postgres", "postgres", "agentdb")

func PublishMessage(rbd *redis.Client, ctx *context.Context, msg []byte, channel string) {
	err := rbd.Publish(*ctx, channel, msg).Err()

	if err != nil {
		fmt.Println("Error: ", err)
	}
}

func PrintMessage(channel *redis.PubSub, ctx *context.Context) {

	msg, err := channel.ReceiveMessage(*ctx)

	if err != nil {
		fmt.Println(err)
	}

	a := &models.AgentMessage{}
	e := a.UnmarshalData([]byte(msg.Payload))

	if e != nil {
		fmt.Println(e)
	}

	fmt.Println(a.Hostname, " ", a.ID, " ")

	// for msg := range channel.Channel() {

	// }

	// if err != nil {
	// 	fmt.Println("Error: ", err)
	// }

	// fmt.Println("Message Received on ", msg.Channel, " : ", msg.Payload)

	// return msg.Payload
}
