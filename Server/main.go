package main

import (
	"context"
	"encoding/json"
	"fmt"
	"go-redis-server/models"
	"go-redis-server/utils"
	"math/rand"
	"strconv"

	"github.com/go-redis/redis/v8"
)

func main() {

	//redis
	rbd := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	fmt.Println("rbd: ", rbd)

	ctx := context.Background()

	channel := rbd.Subscribe(ctx, "channel1")

	for msg := range channel.Channel() {

		a := &models.AgentMessage{}
		e := a.UnmarshalData([]byte(msg.Payload))

		if e != nil {
			fmt.Println(e)
		}

		fmt.Println(a.Hostname, " ", a.ID, " ", a.Msg)

		id := strconv.Itoa(rand.Int())

		ack := models.AgentMessage{Msg: a.Msg, ID: id, Hostname: a.Hostname}

		val, err := json.Marshal(ack)

		if err != nil {
			panic(err)
		}

		utils.PublishMessage(rbd, &ctx, val, "channel2")

		// time.Sleep(3 * time.Second)
	}

}
